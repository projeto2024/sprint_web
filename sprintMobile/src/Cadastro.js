import React, { useState } from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useNavigation } from '@react-navigation/native';

export default function Cadastro() {
  const [form, setForm] = useState({
    nome: '',
    email: '',
    telefone: '',
    password: '',
    confirmarSenha: '',
  });

  const navigation = useNavigation();

  const cadastrarUsuario = () => {
    if (form.nome && form.email && form.telefone && form.password && form.confirmarSenha) {
      if (form.password !== form.confirmarSenha) {
        alert('As senhas não coincidem');
      } else {
        fetch('https://sua-api.com/verificar-email', {
          method: 'POST',
          body: JSON.stringify({ email: form.email }),
          headers: {
            'Content-Type': 'application/json'
          }
        })
        .then(response => response.json())
        .then(data => {
          if (data.emailEmUso) {
            alert('Este email já está em uso');
          } else {
            fetch('https://sua-api.com/cadastrar-usuario', {
              method: 'POST',
              body: JSON.stringify(form),
              headers: {
                'Content-Type': 'application/json'
              }
            })
            .then(response => response.json())
            .then(data => {
              // Lógica após o cadastro, se necessário
              alert('Cadastro realizado com sucesso!');
            })
            .catch(error => {
              console.error('Erro ao cadastrar usuário:', error);
            });
          }
        })
        .catch(error => {
          console.error('Erro ao verificar email:', error);
        });
      }
    } else {
      alert('Preencha todos os campos obrigatórios');
    }
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <KeyboardAwareScrollView>
          <View style={styles.header}>
            <Image
              alt="App Logo"
              resizeMode="contain"
              style={styles.headerImg}
              source={{
                uri: 'https://assets.withfra.me/SignIn.2.png',
              }} />

            <Text style={styles.title}>
              Crie sua <Text style={{ color: '#56409e' }}>conta</Text>
            </Text>

            <Text style={styles.text}>
              Comece a embarcar com a gente!
            </Text>
          </View>

          <View style={styles.form}>
            <View style={styles.input}>
              <Text style={styles.inputLabel}>Nome</Text>
              <TextInput
                onChangeText={nome => setForm({ ...form, nome })}
                placeholder="Seu nome"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                value={form.nome} />
            </View>

            <View style={styles.input}>
              <Text style={styles.inputLabel}>Email</Text>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="email-address"
                onChangeText={email => setForm({ ...form, email })}
                placeholder="email@exemplo.com"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                value={form.email} />
            </View>

            <View style={styles.input}>
              <Text style={styles.inputLabel}>Telefone</Text>
              <TextInput
                keyboardType="phone-pad"
                onChangeText={telefone => setForm({ ...form, telefone })}
                placeholder="(XX) XXXXX-XXXX"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                value={form.telefone} />
            </View>

            <View style={styles.input}>
              <Text style={styles.inputLabel}>Senha</Text>
              <TextInput
                autoCorrect={false}
                onChangeText={password => setForm({ ...form, password })}
                placeholder="********"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                secureTextEntry={true}
                value={form.password} />
            </View>

            <View style={styles.input}>
              <Text style={styles.inputLabel}>Confirmar Senha</Text>
              <TextInput
                autoCorrect={false}
                onChangeText={confirmarSenha => setForm({ ...form, confirmarSenha })}
                placeholder="********"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                secureTextEntry={true}
                value={form.confirmarSenha} />
            </View>

            <View style={styles.formAction}>
              <TouchableOpacity
                onPress={cadastrarUsuario}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>Cadastrar</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 24,
    paddingHorizontal: 0,
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
  },
  title: {
    fontSize: 28,
    fontWeight: "500",
    color: "#281b52",
    textAlign: "center",
    marginBottom: 12,
    lineHeight: 40,
  },
  text: {
    fontSize: 15,
    lineHeight: 24,
    fontWeight: "400",
    color: "#9992a7",
    textAlign: "center",
  },
  header: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 36,
  },
  headerImg: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    marginBottom: 36,
  },
  form: {
    marginBottom: 24,
    paddingHorizontal: 24,
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
  },
  formAction: {
    marginTop: 4,
    marginBottom: 16,
  },
  formLink: {
    fontSize: 16,
    fontWeight: '600',
    color: '#075eec',
    textAlign: 'center',
  },
  formFooter: {
    fontSize: 15,
    fontWeight: '600',
    color: '#222',
    textAlign: 'center',
    letterSpacing: 0.15,
  },
  input: {
    marginBottom: 16,
  },
  inputLabel: {
    fontSize: 17,
    fontWeight: '600',
    color: '#222',
    marginBottom: 8,
  },
  inputControl: {
    height: 50,
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    borderRadius: 12,
    fontSize: 15,
    fontWeight: '500',
    color: '#222',
    borderWidth: 1,
    borderColor: '#C9D3DB',
    borderStyle: 'solid',
  },
  /** Button */
  button: {
    backgroundColor: "#56409e",
    paddingVertical: 12,
    paddingHorizontal: 14,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 12,
  },
  buttonText: {
    fontSize: 15,
    fontWeight: "500",
    color: "#fff",
  },
});
